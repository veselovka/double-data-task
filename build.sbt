name := "DoubleDataTask"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.4.1",
  "org.scalatest" %% "scalatest" % "3.0.0-M7" % "test"
)