package ru.doubledata

import java.nio.file.Paths

import org.apache.spark.SparkContext
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import org.scalatest.Matchers._
import ru.doubledata.SimilarityMatrixApp.{initSparkContext, parse}

class SimilarityMatrixTest extends FunSuite with BeforeAndAfterAll {

  var sc: SparkContext = _

  override def beforeAll() = {
    sc = initSparkContext()
  }

  override def afterAll() = {
    sc.stop()
  }

  test("Testing on dataset 1") {
    runTestOn(
      datasetName = "test_dataset_1.csv",
      expected = Array(
        Array(1, 0, 1, 1, 1),
        Array(0, 4, 2, 2, 2),
        Array(1, 2, 3, 2, 2),
        Array(1, 2, 2, 3, 2),
        Array(1, 2, 2, 2, 3)
      ))
  }

  test("Testing on dataset 2") {
    runTestOn(
      datasetName = "test_dataset_2.csv",
      expected = Array(
        Array(2, 0, 0, 2, 0, 0, 0),
        Array(0, 2, 2, 0, 1, 0, 0),
        Array(0, 2, 2, 0, 1, 0, 0),
        Array(2, 0, 0, 3, 0, 1, 1),
        Array(0, 1, 1, 0, 3, 1, 1),
        Array(0, 0, 0, 1, 1, 2, 1),
        Array(0, 0, 0, 1, 1, 1, 2)
      )
    )
  }

  test("Testing on dataset 3") {
    runTestOn(
      datasetName = "test_dataset_3.csv",
      expected = Array(
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 2, 0, 2, 0, 1, 0, 0, 0, 0),
        Array(0, 0, 2, 0, 1, 0, 2, 0, 0, 0),
        Array(0, 2, 0, 3, 0, 2, 0, 0, 0, 0),
        Array(0, 0, 1, 0, 2, 0, 2, 0, 0, 0),
        Array(0, 1, 0, 2, 0, 2, 0, 0, 0, 0),
        Array(0, 0, 2, 0, 2, 0, 3, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 2, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 1, 1),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 1, 1)
      )
    )
  }

  def runTestOn(datasetName: String, expected: Array[Array[Int]]) {
    val data = dataSet(datasetName)
    val actual = new SimilarityMatrixBuilder(data).matrixAsObj.asArray
    actual should be (expected)
  }

  def dataSet(name: String) = {
    val path = Paths.get(getClass.getResource(s"/$name").toURI).toString
    parse(path, sc)
  }
}
