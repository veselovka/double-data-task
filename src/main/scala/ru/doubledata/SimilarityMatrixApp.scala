package ru.doubledata

import java.nio.file.{Path, Files, Paths}

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import ru.doubledata.SimilarityMatrixApp.smallestFirst

object SimilarityMatrixApp extends App {

  val sc = initSparkContext()

  val path = args.headOption.map(Paths.get(_)).getOrElse(Paths.get(getClass.getResource("/hadoop_dataset.csv").toURI)).toString
  val input = parse(path, sc)
  val outputDir = System.getProperty("java.io.tmpdir")
  new SimilarityMatrixBuilder(input).matrixToFile(outputDir)

  sc.stop()

  def initSparkContext(name: String = "Similarity Matrix Application", master: String = "local[2]") = {
    val conf = new SparkConf().setAppName(name).setMaster(master)
    new SparkContext(conf)
  }

  def parse(path: String, sc: SparkContext) = {
    val rawData = sc.textFile(path)
    rawData.map { line ⇒
      val nums = line.split(' ')
      require(nums.length == 2, "Invalid input data format")
      nums.head.toInt → nums(1).toInt
    }
  }

  def smallestFirst(v1: Int, v2: Int) =
    if (v1 < v2) v1 → v2 else v2 → v1
}

class SimilarityMatrixBuilder(private val data: RDD[(Int, Int)]) {
  private val adjacencyList = data.flatMap {
    case (n1, n2) ⇒ Seq(n1 → Seq(n2), n2 → Seq(n1))
  }.reduceByKey(_ ++ _)

  private val totalFriends = adjacencyList.map {
    case (person, friends) ⇒ person -> friends.size
  }

  private val countOfMutualFriends = adjacencyList.values.flatMap { friends ⇒
    friends.combinations(2).map {
      case Seq(f1, f2) ⇒ smallestFirst(f1, f2) → 1
    }
  }.reduceByKey(_ + _)

  lazy val matrixAsObj = {
    new SimilarityMatrix(
      diagonal = totalFriends.collectAsMap().toMap.withDefaultValue(0),
      elements = countOfMutualFriends.collectAsMap().toMap.withDefaultValue(0)
    )
  }

  def matrixToFile(dir: String) = {
    val totalFriendsPath = Paths.get(dir, "total_friends")
    deleteFile(totalFriendsPath)
    totalFriends.repartition(1).saveAsTextFile(totalFriendsPath.toString)
    val mutualFriendsPath = Paths.get(dir, "mutual_friends")
    deleteFile(mutualFriendsPath)
    countOfMutualFriends.repartition(1).saveAsTextFile(mutualFriendsPath.toString)
    println(s"Total friends saved to $totalFriendsPath; mutual friends saved to $mutualFriendsPath")
  }

  private def deleteFile(path: Path): Unit = {
    import collection.JavaConversions.iterableAsScalaIterable
    if (Files.exists(path)) {
      Files.newDirectoryStream(path).foreach(Files.delete)
      Files.delete(path)
    }
  }
}

class SimilarityMatrix(private val diagonal: Map[Int, Int], private val elements: Map[(Int, Int), Int]) {
  private val dim = diagonal.keys.max // in case when graph contains isolated nodes

  def element(row: Int, col: Int) = {
    require(row < dim && col < dim, "Illegal indices")
    if (row == col) {
      diagonal(row + 1)
    } else {
      val (i, j) = smallestFirst(row, col)
      elements(i + 1, j + 1)
    }
  }

  // don't use for huge matrix
  lazy val asArray = {
    val m = Array.fill[Int](dim, dim)(0)
    diagonal.foreach {
      case (node, value) ⇒ m(node - 1)(node - 1) = value
    }
    elements.foreach {
      case ((id1, id2), value) ⇒
        m(id1 - 1)(id2 - 1) = value
        m(id2 - 1)(id1 - 1) = value
    }
    m
  }

  override def toString = {
    asArray.map { row ⇒
      row.mkString(" ")
    }.mkString("\n")
  }
}